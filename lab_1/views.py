from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Ihwan Edi Saputro' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 7, 18) #TODO Implement this, format (Year, Month, Date)
npm = 1706074934 # TODO Implement this
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': 19, 'npm': npm}
    return render(request, 'challenge.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
